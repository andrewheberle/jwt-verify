FROM golang:1.15 as builder

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /go/src/gitlab.com/andrewheberle/jwt-verify/

# copy source in
COPY . .

# build binary
RUN go build -v -o /go/bin/jwt-verify .

# use a distroless base image with glibc
FROM gcr.io/distroless/base-debian10:nonroot

# copy executable from builder
COPY --from=builder --chown=nonroot /go/bin/jwt-verify /usr/local/bin/

# entrypoint and command
ENTRYPOINT [ "jwt-verify" ]
CMD [ "help" ]
