package cmd

import (
	"fmt"
	"os"

	spoe "github.com/criteo/haproxy-spoe-go"
	"github.com/spf13/cobra"
)

var (
	// flags
	spoeResponse string
)

func init() {
	rootCmd.AddCommand(spoaCmd)

	spoaCmd.Flags().StringVarP(&spoeResponse, "spoe-response", "s", "valid", "variable to set in spoa response")
}

var spoaCmd = &cobra.Command{
	Use:   "spoa",
	Short: "Run as a SPOA for HAProxy",
	Long: `Run as a Stream Processing Offload Protocol (SPOP) compatible
Stream Protocol Offload Agent (SPOA) to verify a JWT on behalf of HAProxy.`,
	Run: func(cmd *cobra.Command, args []string) {
		jwtVerifier = NewVerifier(authDomain)
		agent := spoe.New(jwtValidate)
		if err := agent.ListenAndServe(fmt.Sprintf("%s:%d", listenAddress, listenPort)); err != nil {
			fmt.Fprintf(os.Stderr, "spoa error: %v\n", err)
			os.Exit(1)
		}
	},
}

// jwtValidate function
func jwtValidate(messages *spoe.MessageIterator) ([]spoe.Action, error) {
	var accessJWT, policyAUD string

	for messages.Next() {
		msg := messages.Message

		if msg.Name != "jwt-verify" {
			continue
		}

		// loop through args
		for k, v := range msg.Args.Map() {
			var ok bool

			switch k {
			case "jwt":
				accessJWT, ok = v.(string)
				if !ok {
					return nil, fmt.Errorf("jwt value was not a string")
				}
			case "aud":
				policyAUD, ok = v.(string)
				if !ok {
					return nil, fmt.Errorf("aud value was not a string")
				}
			}
		}

		// check we got the data required
		if accessJWT == "" || policyAUD == "" {
			return nil, fmt.Errorf("missing jwt or aud")
		}

		// validate jwt
		if ok, err := jwtVerifier.Check(accessJWT, policyAUD); !ok {
			return nil, fmt.Errorf("jwt verification failed: %w", err)
		}
	}

	// if we get here the jwt was valid
	return []spoe.Action{
		spoe.ActionSetVar{
			Name:  spoeResponse,
			Scope: spoe.VarScopeTransaction,
			Value: true,
		},
	}, nil
}
