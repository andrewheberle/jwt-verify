package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// globals
	jwtVerifier *Verifier

	// flags
	listenAddress string
	listenPort    int
	authDomain    string

	rootCmd = &cobra.Command{
		Use:   "jwt-verify",
		Short: "jwt-verify is used to verify a provided JWT via SPOP or HTTP",
		Long: `A service to verify a JWT provided by Cloudflare Access via
HAProxy using the Stream Processing Offload Protocol or
nginx using a HTTP sub-request`,
	}
)

func init() {
	// command line flag handling
	rootCmd.PersistentFlags().StringVarP(&authDomain, "auth-domain", "d", "", "authentication domain (.cloudflareaccess.com is appended)")
	rootCmd.PersistentFlags().StringVarP(&listenAddress, "address", "l", "127.0.0.1", "listen address")
	rootCmd.PersistentFlags().IntVarP(&listenPort, "port", "p", 9000, "listen port")

	// required flags
	_ = rootCmd.MarkPersistentFlagRequired("auth-domain")

	// hook flags into viper
	_ = viper.BindPFlag("auth-domain", rootCmd.PersistentFlags().Lookup("auth-domain"))
	_ = viper.BindPFlag("address", rootCmd.PersistentFlags().Lookup("address"))
	_ = viper.BindPFlag("port", rootCmd.PersistentFlags().Lookup("port"))

	// accept from environment also
	viper.SetEnvPrefix("jwt")
	viper.AutomaticEnv()
}

// Execute is the entrypoint into the cli app
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
