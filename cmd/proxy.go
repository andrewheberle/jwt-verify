package cmd

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

var (
	// flags
	proxyUri, audValue, certFile, keyFile string
	httpOnly                              bool
)

func init() {
	// add our command
	rootCmd.AddCommand(proxyCmd)

	// flags
	proxyCmd.Flags().StringVarP(&proxyUri, "proxy-uri", "u", "http://127.0.0.1", "uri of service to proxy for")
	proxyCmd.Flags().StringVarP(&jwtHttpHeader, "jwt-header", "j", "Cf-Access-Jwt-Assertion", "header that contains the jwt")
	proxyCmd.Flags().StringVarP(&audValue, "aud", "a", "", "audience value for service")
	proxyCmd.Flags().StringVarP(&certFile, "cert", "c", "", "TLS/SSL certificate file")
	proxyCmd.Flags().StringVarP(&certFile, "key", "k", "", "TLS/SSL private key file")
	proxyCmd.Flags().BoolVar(&httpOnly, "http-only", false, "Run in HTTP only mode (no TLS)")

	// required flags
	_ = proxyCmd.MarkFlagRequired("aud")
}

var proxyCmd = &cobra.Command{
	Use:   "proxy",
	Short: "Run as a HTTP proxy",
	Long:  `Run as a HTTP reverse-proxy to verify a JWT on behalf of another service.`,
	Run: func(cmd *cobra.Command, args []string) {
		jwtVerifier = NewVerifier(authDomain)

		url, err := url.Parse(proxyUri)
		if err != nil {
			fmt.Fprintf(os.Stderr, "proxy error: %v\n", err)
			os.Exit(1)
		}
		proxy := httputil.NewSingleHostReverseProxy(url)
		r := mux.NewRouter()
		r.PathPrefix("/").Handler(proxy)
		r.Use(func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				r.Header.Set(audHttpHeader, audValue)
				next.ServeHTTP(w, r)
			})
		})
		r.Use(jwtVerifier.Middleware)
		r.Use(func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				r.Header.Del(audHttpHeader)
				next.ServeHTTP(w, r)
			})
		})
		if httpOnly {
			if err := http.ListenAndServe(fmt.Sprintf("%s:%d", listenAddress, listenPort), r); err != nil {
				fmt.Fprintf(os.Stderr, "proxy error: %v\n", err)
				os.Exit(1)
			}
		} else {
			if err := http.ListenAndServeTLS(fmt.Sprintf("%s:%d", listenAddress, listenPort), certFile, keyFile, r); err != nil {
				fmt.Fprintf(os.Stderr, "proxy error: %v\n", err)
				os.Exit(1)
			}
		}
	},
}
