package cmd

import (
	"context"
	"fmt"
	"net/http"

	"github.com/coreos/go-oidc"
)

type Verifier struct {
	ctx        context.Context
	keySet     oidc.KeySet
	authDomain string
}

func NewVerifier(authdomain string) *Verifier {
	v := &Verifier{
		ctx:        context.TODO(),
		authDomain: authdomain,
	}
	v.keySet = oidc.NewRemoteKeySet(v.ctx, fmt.Sprintf("https://%s.cloudflareaccess.com/cdn-cgi/access/certs", authdomain))
	return v
}

func (v *Verifier) Check(jwt, aud string) (bool, error) {
	// create a verifier
	verifier := oidc.NewVerifier(fmt.Sprintf("https://%s.cloudflareaccess.com", v.authDomain), v.keySet, &oidc.Config{ClientID: aud})

	// verify jwt
	_, err := verifier.Verify(v.ctx, jwt)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (v *Verifier) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessJWT := r.Header.Get(jwtHttpHeader)
		policyAUD := r.Header.Get(audHttpHeader)

		if accessJWT == "" || policyAUD == "" {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		// validate jwt
		if ok, _ := jwtVerifier.Check(accessJWT, policyAUD); !ok {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
