package cmd

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

var (
	// flags
	jwtHttpHeader, audHttpHeader string
)

func init() {
	rootCmd.AddCommand(httpCmd)

	httpCmd.Flags().StringVarP(&jwtHttpHeader, "jwt-header", "j", "Cf-Access-Jwt-Assertion", "header that contains the jwt")
	httpCmd.Flags().StringVarP(&audHttpHeader, "aud-header", "a", "X-Jwt-Audience", "header that contains the audience value")
}

var httpCmd = &cobra.Command{
	Use:   "http",
	Short: "Run as a HTTP sub-request service for nginx",
	Long: `Run as a HTTP service to verify a JWT on behalf of nginx via a
HTTP sub-request.`,
	Run: func(cmd *cobra.Command, args []string) {
		jwtVerifier = NewVerifier(authDomain)

		r := mux.NewRouter()
		r.HandleFunc("/auth", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
		})
		r.Use(jwtVerifier.Middleware)
		if err := http.ListenAndServe(fmt.Sprintf("%s:%d", listenAddress, listenPort), r); err != nil {
			fmt.Fprintf(os.Stderr, "http error: %v\n", err)
			os.Exit(1)
		}
	},
}
