package main

import (
	"gitlab.com/andrewheberle/jwt-verify/cmd"
)

func main() {
	cmd.Execute()
}
