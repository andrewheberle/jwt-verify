variables:
  GO111MODULE: 'on'
  GOLINT_VERSION: 'v1.32.2'
  GOARCH: 'amd64'
  
default:
  image: golang:1.15
  cache:
    paths:
      - .cache
  before_script:
    - mkdir -p .cache/bin
    - export GOPATH="$CI_PROJECT_DIR/.cache"

stages:
  - test
  - build
  - release

testing:
  stage: test
  script:
    - curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin $GOLINT_VERSION
    - .cache/bin/golangci-lint --deadline 15m run
    - go vet
    - go test -race -short
    - go test -short -covermode=count

linux-glibc-amd64:
  stage: build
  variables:
    GOOS: 'linux'
  script:
    - mkdir -p "${GOOS}-glibc-${GOARCH}"
    - go build -o "${GOOS}-glibc-${GOARCH}/jwt-verify" .
  artifacts:
    paths:
      - '${GOOS}-glibc-${GOARCH}/jwt-verify'

linux-musl-amd64:
  stage: build
  image: golang:1.15-alpine
  variables:
    GOOS: 'linux'
  script:
    - mkdir -p "${GOOS}-musl-${GOARCH}"
    - go build -o "${GOOS}-musl-${GOARCH}/jwt-verify" .
  artifacts:
    paths:
      - '${GOOS}-musl-${GOARCH}/jwt-verify'

release:
  stage: release
  image: alpine
  script: |
    apk add --no-cache curl jq
    printf '{ "name": "%s Release", ' "${CI_COMMIT_TAG}" > release.json
    printf '"tag_name": "%s", ' "${CI_COMMIT_TAG}" >> release.json
    printf '"description": %s, ' "$(jq --null-input --compact-output --arg msg "${CI_COMMIT_MESSAGE}" '$msg')" >> release.json
    printf '"assets": { "links": [ ' >> release.json
    printf '{ "name": "linux-musl-%s/jwt-verify", ' "${GOARCH}" >> release.json
    printf '"url": "%s/-/jobs/%s/artifacts/raw/linux-musl-%s/jwt-verify" }, ' "${CI_PROJECT_URL}" "${CI_JOB_ID}" "${GOARCH}" >> release.json
    printf '{ "name": "linux-glibc-%s/jwt-verify", ' "${GOARCH}" >> release.json
    printf '"url": "%s/-/jobs/%s/artifacts/raw/linux-glibc-%s/jwt-verify" } ] } }' "${CI_PROJECT_URL}" "${CI_JOB_ID}" "${GOARCH}" >> release.json
    curl -H "Content-Type: application/json" -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -X POST --data @release.json "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"
  artifacts:
    paths:
      - linux-musl-${GOARCH}/jwt-verify
      - linux-glibc-${GOARCH}/jwt-verify
  only:
    - tags

docker-build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
  except:
    - master

docker-build-master:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master
