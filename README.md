# JWT SPOA

This is a service to validate a request has a valid JSON Web Token (JWT) for [HAProxy](https://www.haproxy.org) or NGINX.

Usage with HAProxy runs as a Stream Processing Offload Agent (SPOA) while integration with NGINX is via a HTTP sub-request.

In its current form, this SPOA is specifically targeted at verifiying JWT's issued by Cloudflare Access to secure on-premise web applications.

Documentation for Cloudflare Access and the use of JSON Web Tokens is here:

[https://developers.cloudflare.com/access/advanced-management/validating-json](https://developers.cloudflare.com/access/advanced-management/validating-json)

Full documentation on this HAProxy feature is here:

[https://www.haproxy.org/download/2.2/doc/SPOE.txt](https://www.haproxy.org/download/2.2/doc/SPOE.txt)

Integration details for NGINX are here:

[https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-subrequest-authentication/](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-subrequest-authentication/)

## Usage

### Command line flags

```sh
Usage:
  jwt-verify [command]

Available Commands:
  help        Help about any command
  http        Run as a HTTP sub-request service for nginx
  spoa        Run as a SPOA for HAProxy

Flags:
  -l, --address string       listen address (default "127.0.0.1")
  -d, --auth-domain string   authentication domain (.cloudflareaccess.com is appended)
  -h, --help                 help for jwt-verify
  -p, --port int             listen port (default 9000)
```

* `address`: This is the address for the SPOA to listen on. This should NOT be publically accessible.
* `auth-domain`: The Cloudflare Access authentication domain (only the part before `.cloudflareaccess.com` should be entered).
* `port`: The TCP port to listen on.

#### Running as a Stream Processing Offload Agent (SPOA)

```sh
jwt-verify spoa --help
Run as a Stream Processing Offload Protocol (SPOP) compatible
Stream Protocol Offload Agent (SPOA) to verify a JWT on behalf of HAProxy.

Usage:
  jwt-verify spoa [flags]

Flags:
  -h, --help                   help for spoa
  -s, --spoe-response string   variable to set in spoa response (default "valid")

Global Flags:
  -l, --address string       listen address (default "127.0.0.1")
  -d, --auth-domain string   authentication domain (.cloudflareaccess.com is appended)
  -p, --port int             listen port (default 9000)

```

* `spoe-reponse`: The variable name to set for a JWT that passes signature verification. This is a transaction (txn) level variable.

#### Running as a sub-request authenticator for NGINX

```sh
jwt-verify http --help
Run as a HTTP service to verify a JWT on behalf of nginx via a
HTTP sub-request.

Usage:
  jwt-verify http [flags]

Flags:
  -h, --help                 help for http
  -j, --http-header string   header that contains the jwt (default "Cf-Access-Jwt-Assertion")

Global Flags:
  -l, --address string       listen address (default "127.0.0.1")
  -d, --auth-domain string   authentication domain (.cloudflareaccess.com is appended)
  -p, --port int             listen port (default 9000)

```

* `http-header`: The HTTP header that contains the JSON Web Token (JWT)

#### Running as a reverse proxy

```sh
jwt-verify proxy --help
Run as a HTTP reverse-proxy to verify a JWT on behalf of another service.

Usage:
  jwt-verify proxy [flags]

Flags:
  -a, --aud string          audience value for service
  -h, --help                help for proxy
  -j, --jwt-header string   header that contains the jwt (default "Cf-Access-Jwt-Assertion")
  -u, --proxy-uri string    uri of service to proxy for (default "http://127.0.0.1")

Global Flags:
  -l, --address string       listen address (default "127.0.0.1")
  -d, --auth-domain string   authentication domain (.cloudflareaccess.com is appended)
  -p, --port int             listen port (default 9000)
```

### Environment Variables

All configuration options that can be specified on the command line can be provided by environment variables:

* `JWT_ADDRESS`: listen address
* `JWT_AUTH-DOMAIN`: authentication domain
* `JWT_PORT`: listen port
* `JWT_SPOE-RESPONSE`: spoe response variable name

Please Note: *This functionality is currently broken*

### Configuration

#### HAProxy

Configure HAProxy:

```haproxy
# example HTTPS listener
listen https
    bind :443 ssl crt /etc/haproxy/server.pem alpn h2,http/1.1
    mode http

    balance roundrobin
    timeout connect 5s
    timeout client  30s
    timeout server  30s

    # load spoe config
    filter spoe engine jwt-verify config /etc/haproxy/spoe.cfg

    # set audience value for application and send to spoe-group
    http-request set-var(txn.aud) str("<audience value for service>")
    http-request send-spoe-group jwt-verify jwt-verify

    # only allow requests with a valid jwt by checking the variable set
    http-request deny if ! { var(txn.jwt_verify.valid) -m bool }

    server server1 <IP ADDRESS>:<PORT> ssl check

# our SPOA
backend spoe
    mode tcp

    balance roundrobin
    timeout connect 5s
    timeout server  3m

    server jwt-spoa 127.0.0.1:9000
```

Create `/etc/haproxy/spoe.cfg`:

```haproxy
[jwt-verify]
spoe-agent jwt-verify
    log         global
    option      var-prefix jwt_verify

    timeout     hello      2s
    timeout     processing 10ms
    timeout     idle       2m

    groups      jwt-verify

    use-backend spoe

spoe-message jwt-verify
    args        jwt=req.hdr(cf-access-jwt-assertion)
    args        aud=var(txn.aud)

spoe-group jwt-verify
    messages    jwt-verify
```

Start JWT Verify in SPOA mode:

```sh
jwt-verify --auth-domain <ACCESS AUTH DOMAIN> spoa
```

Reload HAProxy configuration:

```sh
systemctl reload haproxy
```

#### NGINX

Configure NGINX:

```nginx
http {
    #...
    server {
    #...
        location /private/ {
            auth_request     /auth;
            auth_request_set $auth_status $upstream_status;
        }

        location = /auth {
            internal;
            proxy_pass              http://127.0.0.1:9000;
            proxy_pass_request_body off;
            proxy_set_header        Content-Length "";
            proxy_set_header        X-Original-URI $request_uri;
            proxy_set_header        X-Jwt-Audience "<audience value for service>";
        }
    }
}
```

Start JWT Verify in HTTP mode:

```sh
jwt-verify --auth-domain <ACCESS AUTH DOMAIN> http
```

Reload NGINX configuration:

```sh
systemctl reload nginx
```

## Running as a service

An example systemd unit file is can be built and used as follows:

```sh
make -C systemd jwt-verify-spoa@.service
cp systemd/jwt-verify-spoa@.service /etc/systemd/system
useradd -d / -M -s /sbin/nologin jwt-verify
systemctl daemon-reload
systemctl enable jwt-verify-spoa@<ACCESS AUTH DOMAIN>.service
systemctl start jwt-verify-spoa@<ACCESS AUTH DOMAIN>.service
```

## Limitations

The service does nothing with any claims within the JWT, it only verifies the
signature is valid.

As noted above, this is currently very Cloudflare Access centric.
